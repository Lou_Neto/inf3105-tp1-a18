# Makefile pour INF3105 / TP1.
# Adaptez ce fichier si nécessaire.

# Choisir l'une des ces trois configurations:
OPTIONS = -Wall
#OPTIONS = -g -O0 -Wall
#OPTIONS = -O2 -Wall

all : tp1

tp1 : tp1.cpp heure.o pointst.o personne.o tableau.h
	g++ ${OPTIONS} -o tp1 tp1.cpp heure.o personne.o pointst.o
	
pointst.o : pointst.cpp pointst.h
	g++ ${OPTIONS} -c -o pointst.o pointst.cpp

heure.o : heure.cpp heure.h
	g++ ${OPTIONS} -c -o heure.o heure.cpp

personne.o : personne.cpp personne.h pointst.h
	g++ ${OPTIONS} -c -o personne.o personne.cpp

clean:
	rm -rf tp1 *~ *.o *+.txt *.txt

