/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "heure.h"
Heure::Heure(const Heure& autre) 
    : heures(autre.heures), minutes(autre.minutes)
{
}

bool Heure::operator <(const Heure& h) const{
    return ( h.heures <= heures && h.minutes < minutes );
}

Heure Heure::calculerHeure(const PointST& origine,const PointST& destination,Heure& h){
    double distanceParcouru = distance(origine ,destination);
    double nbSeconde = distanceParcouru / 10;
    int nbMinutes = round(nbSeconde / 60);
    int nbHeures = round(nbMinutes/ 60);
    h.heures += nbHeures;
    if(nbMinutes + h.minutes >= 60){
        h.heures++;
        h.minutes = h.minutes % 60;
    } else{
        h.minutes += nbMinutes;
    }

    return h;
}

std::istream& operator >> (std::istream& is, Heure& heure){
	// À compléter.
    int heures, minutes;
    char h;
    is >> heures >> h >> minutes;
    heure.heures = heures;
    heure.minutes = minutes;
    if(!is) return is;
    assert(h=='h' || h=='H'); // pour robustesse: valider le caractère 'h'
    return is;
}

std::ostream& operator << (std::ostream& os, const Heure& h){
	// À compléter.
    int minutes = h.minutes;
    int heures = h.heures;
    char chaine[40];
    sprintf(chaine, "%02dh%02d", heures, minutes);
    os << chaine;
    return os;
}
