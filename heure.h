/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

#if !defined(__HEURE_H__)
#define __HEURE_H__

#include <iostream>
#include "pointst.h"
#include "tableau.h"


class Heure{
  public:
	// À compléter.
    Heure() : heures(0), minutes(00){}
    Heure(int heure, int minutes) : heures(heure), minutes(minutes){};
    Heure(const Heure&);
    
    bool operator <(const Heure&) const;
    Heure calculerHeure(const PointST&,const PointST&,Heure& H);
    int heures, minutes;

  private:



	// À compléter.

  friend std::istream& operator >> (std::istream&, Heure&);
  friend std::ostream& operator << (std::ostream&, const Heure&);
};

#endif

