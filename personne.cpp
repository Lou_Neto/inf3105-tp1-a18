/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

#include <cassert>
#include "personne.h"

Personne& Personne::operator = (const Personne& autre){
    nom = autre.nom;
    
    o = autre.o;
    d = autre.d;
    
    pretVehicule = autre.pretVehicule;
    nombrePassagers = autre.nombrePassagers;

    departResidence = autre.departResidence;
    arriveDestination = autre.arriveDestination;
    departDestination = autre.departDestination;
    arriveResidence = autre.arriveResidence;

    return *this;
}

bool Personne::operator == (const Personne& autre){
    return (nom == autre.nom && o == autre.o && d == autre.d);
}

std::istream& operator >> (std::istream& is, Personne& p){
	// À compléter.
    std::string s;
    char c;
    // lire nom, nombre de passagers, l'acceptation de prêter le véhicule
    is >> p.nom >> p.nombrePassagers >> p.pretVehicule;
    // lire l'origine (résidence) et la destination (travail / étude)
    is >> p.o >> s >> p.d;
    assert(s == "-->"); // robustesse
    // lire les contraintes de temps (4 heures)
    is >> p.departResidence 
       >> p.arriveDestination 
       >> p.departDestination 
       >> p.arriveResidence;
    // lire le point-virgule de fin de ligne
    is >> c;
    assert(c== ';'); // robustesse
    return is;
}

std::ostream& operator << (std::ostream& os, const Personne& p){
	// À compléter.
    return os;
}

