/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

#if !defined(__PERSONNE_H__)
#define __PERSONNE_H__

#include <iostream>
#include "heure.h"
#include "pointst.h"

class Personne{
  public:
	// À compléter.
		Personne(){}

		PointST o, d;
		std::string nom;

		char pretVehicule;
		int nombrePassagers;

		Heure departResidence;
		Heure arriveDestination;
		Heure departDestination;
		Heure arriveResidence;

		Personne&    operator = (const Personne& autre);
		bool				 operator ==(const Personne& autre);

  private:
	// À compléter.

  friend std::istream& operator >> (std::istream&, Personne&);
  friend std::ostream& operator << (std::ostream&, const Personne&);
};

#endif

