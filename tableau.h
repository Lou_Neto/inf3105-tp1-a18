/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Classe générique Tableau<T> pour le TP1 et Lab3.

   AUTEUR(S):
    1) Lou-Gomes Neto + NETL14039105
    2) Hugo Twigg-Côté + TWIH25048700
*/

#if !defined(_TABLEAU___H_)
#define _TABLEAU___H_

#include <assert.h>

template <class T>
class Tableau{

  public:
    Tableau(int capacite_initiale=4);
    Tableau(const Tableau&);
    ~Tableau();

    // Ajouter un element à la fin
    void           ajouter(const T& element);
    // redimensionner le tableau
    void           redimensionner(int nouvelleCapacite);
    // Vider le tableau
    void           vider();
    // Retourne le nombre d'éléments dans le tableau
    int            taille() const;

    // Insère element à position index. Les éléments à partir de index sont décalés d'une position.
    void           inserer(const T& element, int index=0);

    // Enlève l'element à position index. Les éléments après index sont décalés d'une position après.
    void           enlever(int index=0);

    // Cherche et retourne la position de l'élément. Si non trouvé, retourne -1.
    int            chercher(const T& element);

    const T&       operator[] (int index) const;
    T&             operator[] (int index);

    bool           operator == (const Tableau<T>& autre) const;
    Tableau<T>&    operator = (const Tableau<T>& autre);

  private:
    T*             elements;
    int            nbElements;
    int            capacite;
};


// ---------- Définitions -------------


template <class T>
Tableau<T>::Tableau(int capacite_)
{
    capacite = capacite_;
    nbElements = 0;
    elements = new T[capacite_];
}

template <class T>
Tableau<T>::Tableau(const Tableau& autre)
{
    capacite = autre.capacite;
    nbElements = autre.nbElements;
    elements = new T[capacite];
    for(int i = 0; i < nbElements; i++)
        elements[i] = autre.elements[i];
}

template <class T>
Tableau<T>::~Tableau()
{
    delete[] elements;
    elements = NULL;
}

template <class T>
int Tableau<T>::taille() const
{
    return nbElements;
}

template <class T>
void Tableau<T>::ajouter(const T& item)
{
    if(nbElements >= capacite)
    {
        redimensionner(capacite*2);
    }    
    elements[nbElements++] = item;
}

template <class T>
void Tableau<T>::redimensionner(int nouvelleCapacite)
{
    capacite = nouvelleCapacite;
    T* temp = new T[capacite];
    for(int i = 0; i < nbElements; i++)
    {
        temp[i] = elements[i];
    }
    delete[] elements;
    elements = temp;
}

template <class T>
void Tableau<T>::inserer(const T& element, int index)
{
    assert(index < nbElements);
    if(nbElements >= capacite)
    {
        redimensionner(capacite*2);
    }

    for(int i = nbElements-1; i >= index; i--)
        elements[i+1] = elements[i];
    elements[index] = element;
    nbElements++;
}

template <class T>
void Tableau<T>::enlever(int index)
{
    for(int i = index; i < nbElements; i++)
        elements[i] = elements[i+1];
    nbElements--;
}

template <class T>
int Tableau<T>::chercher(const T& element)
{
    int position = -1;
    for(int i = 0; i < nbElements; i++)
        if(elements[i] == element) position = i;

    return position;
}

template <class T>
void Tableau<T>::vider()
{
    nbElements = 0;
    // delete[] elements;
    // elements = new T[capacite];
}

template <class T>
const T& Tableau<T>::operator[] (int index) const
{
    assert(index < nbElements);
    return elements[index];
}

template <class T>
T& Tableau<T>::operator[] (int index)
{
    assert(index < nbElements);
    return elements[index];
}

template <class T>
Tableau<T>& Tableau<T>::operator = (const Tableau<T>& autre)
{
    if(this == &autre) return *this;
    nbElements = autre.nbElements;
    if(capacite < autre.nbElements)
    {
        delete[] elements;
        capacite = autre.nbElements;
        elements = new T[capacite];
    }
    for(int i = 0; i < nbElements; i++)
        elements[i] = autre.elements[i];
    return *this;
}

template <class T>
bool Tableau<T>::operator == (const Tableau<T>& autre) const
{
    if(this == &autre) return true;
    if(nbElements != autre.nbElements) return false;
    for(int i = 0; i < nbElements; i++)
        if(elements[i] != autre.elements[i]) return false;
    return true;
}

#endif //define _TABLEAU___H_
