/*  INF3105 - Structures de données et algorithmes       *
 *  UQAM / Département d'informatique                    *
 *  Automne 2018 / TP1                                   *
 *  http://ericbeaudry.uqam.ca/INF3105/tp1/              */

#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include "personne.h"
#include "tableau.h"
#include "heure.h"
#define VITESSE_VOITURE 10

int tp1(std::istream &entree);
void afficherRecommandations(const Tableau<Personne> &);
void outputRecommandation(bool pretDeVoiture,
                          int p1,
                          int meilleureRecommandation,
                          double meilleurCout,
                          const Tableau<Personne> &);
bool calculerHoraire(const Tableau<Personne> &, int p1, int p2, bool acceptePret);

// argc: nombre d'arguments passés.
// argv: tableau de chaines de carractères.
// exemple: ./tp1 test00.txt
//   ==> argc=2; argv[0]="./tp1"; argv[1]="test00.txt"
int main(int argc, const char **argv)
{
    if (argc > 1)
    {
        std::ifstream fichier_entree(argv[1]);
        if (fichier_entree.fail())
        {
            std::cerr << "Erreur de lecture du fichier '" << argv[1] << "'" << std::endl;
            return 1;
        }
        return tp1(fichier_entree);
    }
    else
    {
        // Pas dargument requêtes ==> lecture depuis l'entrée standard avec std::cin.
        return tp1(std::cin);
    }
}

int tp1(std::istream &entree_requetes)
{
    //Structure pour stocker les personnes. Indice: on devrait utiliser le Tableau.
    Tableau<Personne> personnes;

    //Lecture
    while (entree_requetes && !entree_requetes.eof())
    {
        Personne p;
        // Lecture ajout au tableau d'une personne
        entree_requetes >> p >> std::ws;
        personnes.ajouter(p);
    }

    afficherRecommandations(personnes);

    return 0;
}

void afficherRecommandations(const Tableau<Personne> &personnes)
{

    double meilleurCout;
    int meilleureRecommandation;
    for (int p1 = 0; p1 < personnes.taille(); p1++)
    {
            bool troisPersonnes = (personnes[p1].nombrePassagers == 3);

        meilleurCout = 2 * distance(personnes[p1].o, personnes[p1].d);
        meilleureRecommandation = p1;
        double distancePret, distanceSansPret, meilleureDistancePret, meilleureDistanceSansPret, cout;
        bool acceptePret = (personnes[p1].pretVehicule == 'O'), pretDeVoiture = false;

        for (int p2 = 0; p2 < personnes.taille(); p2++)
        {
            if (p1 == p2) continue;

            bool estEnRetard = calculerHoraire(personnes,p1,p2,acceptePret);

            if (estEnRetard) 
            continue;

            distanceSansPret = 2 * (distance(personnes[p1].o, personnes[p2].o) 
                                    + distance(personnes[p2].d, personnes[p1].d));

            distancePret = 2 * (distance(personnes[p1].o, personnes[p2].o) 
                                + distance(personnes[p2].o, personnes[p1].d) 
                                + distance(personnes[p1].d, personnes[p2].d) 
                                - distance(personnes[p2].o, personnes[p2].d));

            if (distancePret <= distanceSansPret && acceptePret)
            {
                cout = distancePret;
            }
            else
            {
                cout = distanceSansPret;
            }

            if (cout < meilleurCout)
            {
                meilleurCout = cout;
                meilleureRecommandation = p2;
                meilleureDistancePret = distancePret;
                meilleureDistanceSansPret = distanceSansPret;
            }
        }

        pretDeVoiture = (meilleureDistancePret <= meilleureDistanceSansPret && acceptePret) ? true : false;

        outputRecommandation(pretDeVoiture, p1, meilleureRecommandation, meilleurCout, personnes);
    }
}

void outputRecommandation(bool pretDeVoiture,
                          int p1,
                          int meilleureRecommandation,
                          double meilleurCout,
                          const Tableau<Personne> &personnes)
{

    if (p1 == meilleureRecommandation)
    {
        std::cout << personnes[p1].nom << ":\t"
                  << "+" << personnes[p1].nom << "-" << personnes[p1].nom
                  << "\t"
                  << "+" << personnes[p1].nom << "-" << personnes[p1].nom
                  << "\t" << round(meilleurCout) << "m"
                  << std::endl;
    }
    else
    {
        if (pretDeVoiture)
        {
            std::cout << personnes[p1].nom << ":\t"
                      << "+" << personnes[p1].nom << "+" << personnes[meilleureRecommandation].nom
                      << "-" << personnes[p1].nom << "-" << personnes[meilleureRecommandation].nom
                      << "\t"
                      << "+" << personnes[meilleureRecommandation].nom << "+" << personnes[p1].nom
                      << "-" << personnes[meilleureRecommandation].nom << "-" << personnes[p1].nom
                      << "\t" << round(meilleurCout) << "m"
                      << std::endl;
        }
        else
        {
            std::cout << personnes[p1].nom << ":\t"
                      << "+" << personnes[p1].nom << "+" << personnes[meilleureRecommandation].nom
                      << "-" << personnes[meilleureRecommandation].nom << "-" << personnes[p1].nom
                      << "\t"
                      << "+" << personnes[p1].nom << "+" << personnes[meilleureRecommandation].nom
                      << "-" << personnes[meilleureRecommandation].nom << "-" << personnes[p1].nom
                      << "\t" << round(meilleurCout) << "m"
                      << std::endl;
        }
    }
}

bool calculerHoraire(const Tableau<Personne> &personnes, int p1, int p2, bool acceptePret)
{

    bool resultat = false;
    Heure tempsParcouru(personnes[p1].departResidence);
   
    if (acceptePret)
    {
        tempsParcouru.calculerHeure(personnes[p1].o, personnes[p2].o, tempsParcouru);
        tempsParcouru.calculerHeure(personnes[p2].o, personnes[p1].d, tempsParcouru);
        tempsParcouru.calculerHeure(personnes[p1].d, personnes[p2].d, tempsParcouru);

    }
    else
    {
        tempsParcouru.calculerHeure(personnes[p1].o, personnes[p2].o, tempsParcouru);
        tempsParcouru.calculerHeure(personnes[p2].o, personnes[p2].d, tempsParcouru);
        tempsParcouru.calculerHeure(personnes[p2].d, personnes[p1].d, tempsParcouru);
    }

    if (tempsParcouru < personnes[p1].arriveDestination){
        resultat = true;
    }
    return resultat;
}
/*  personnes[p1].departResidence;   // the beginning
    personnes[p1].arriveDestination; // start of work
    personnes[p1].departDestination; // leave work
    personnes[p1].arriveResidence;   // the end

    personnes[p2].departResidence;   // the beginning
    personnes[p2].arriveDestination; // start of work
    personnes[p2].departDestination; // leave work
    personnes[p2].arriveResidence;   // the end */